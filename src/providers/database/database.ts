import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { MsgProvider } from '../msg/msg';
import { BehaviorSubject } from 'rxjs/Rx';
import { Platform } from 'ionic-angular';
import { Http } from '@angular/http';
//import { Storage } from '@ionic/storage';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
@Injectable()
export class DatabaseProvider {
  database: SQLiteObject;
  private databaseReady: BehaviorSubject<boolean>;

  constructor(
    private sqlite: SQLite,
    private msg: MsgProvider,
    public sqlitePorter: SQLitePorter,
    //private storage: Storage,
    private platform: Platform,
   // private http: Http
  ) {
    this.createTable();
  }

  getOrGenerateDB() {
    return new Promise((resolve, reject) => {
      this.databaseReady = new BehaviorSubject(false);
      this.platform.ready().then(() => {
        if (this.database != null) {
          resolve(this.database)
        } else {
          this.sqlite.create({
            name: 'db_tam_live_local.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            this.database = db;
            resolve(this.database)
          }).catch((e) => {
            this.msg.msg_plainmsg("ERROR database provider");
            this.msg.msg_plainmsg(JSON.stringify(e));
            reject(e);
          });
        }
      });
    });
  }
  
  createTable() {
    this.getOrGenerateDB().then((db: SQLiteObject) => {
      var sql = "CREATE TABLE IF NOT EXISTS work_order("+
        "id_wo INTEGER PRIMARY KEY AUTOINCREMENT,"+
        "id_tiket INTEGER,no_wo TEXT,id_jenis_kunjungan INTEGER,"+
        "keterangan TEXT,datecreated DATETIME,STATUS INTEGER,"+
        "sign_path TEXT,id_rs INTEGER,approved INTEGER,approved_by INTEGER,"+
        "kode_karyawan INTEGER,Nama_customer TEXT,kode_customer TEXT)";
       // var sql = "DROP TABLE work_order";
      db.executeSql(sql, [])
      .then(() =>console.log("berhasil membuat table"))
      .catch(e => this.msg.msg_plainmsg(JSON.stringify(e)));
      
      var sql = "CREATE TABLE IF NOT EXISTS dashboard_wo_header("+
      "id_dashboard_wo_header INTEGER PRIMARY KEY AUTOINCREMENT,"+
      "id_wo INTEGER,open_time DATETIME,closed_time DATETIME,"+
      "open_by INTEGER,closed_by INTEGER)"
     
      db.executeSql(sql, [])
      .then(() => console.log("berhasil membuat table 2"))
      .catch(e => this.msg.msg_plainmsg(JSON.stringify(e)));
    });
/*
    var sql = "CREATE TABLE IF NOT EXISTS developer(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,skill TEXT,yearsOfExperience INTEGER)";
    db.executeSql(sql, [])
    .then(() => console.log("Berhasil membuat table developer"))
    .catch(e => this.msg.msg_plainmsg(JSON.stringify(e)));
    });
     this.getOrGenerateDB().then((db: SQLiteObject) => { 
         var sql = 'create table IF NOT EXISTS buku (' +
         'id INTEGER PRIMARY KEY AUTOINCREMENT, '+ 
         'judul VARCHAR(255), '+
         'penerbit VARCHAR(50), '+  
         'pengarang VARCHAR(50))';
 
         db.executeSql(sql, [])
             .then(() => this.msg.msg_plainmsg("Tabel buku berhasil dicreate"))
             .catch(e => this.msg.msg_plainmsg(JSON.stringify(e)));
 
         // db.executeSql('DELETE FROM buku', {})
         //     .then(() => console.log('tabel buku berhasil di delete'))
         //     .catch(e => console.log(e));
 
         // db.executeSql('INSERT INTO buku values (?, ?,?,?)', 
         //               [null, "judullll", "penerbittt", "ppengarang OKEOKE"])
         //     .then(() => console.log('tabel buku berhasil di tambah'))
         //     .catch(e => console.log(e));
 
 
         // Untuk create table lainnya bisa ditambahkan di sini
         // contoh
         // var sql = 'create Tabel 2 IF NOT EXISTS delivery_form (' +
         //     'number VARCHAR(255) PRIMARY KEY, '+ 
         //     'round_id VARCHAR(255), '+
         //     'state VARCHAR(50), '+  
         //     'has_returns INTEGER(2), '+  
         //     'has_annotations INTEGER(2))';
         // db.executeSql(sql, {})
         //     .then(() => console.log('delivery_form table has created'))
         //     .catch(e => console.log(e));
     })*/
  }
}
