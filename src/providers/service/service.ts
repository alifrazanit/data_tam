import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { MsgProvider } from '../../providers/msg/msg';
import { ApiProvider } from '../../providers/api/api';
import { App,ToastController, Loading, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginsPage } from '../../pages/logins/logins';
import { BerandaPage } from '../../pages/beranda/beranda';


@Injectable()
export class ServiceProvider {
  loading: Loading;
  constructor(
    private api: ApiProvider,
    private loadingCtrl: LoadingController,
    public app: App,
    private storage: Storage,
    private msg:MsgProvider,
    public http: Http,
    private toast:ToastController
  ) { }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  postData(credentials,apiUrllog, type) { //post data berhasil
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(apiUrllog + type, JSON.stringify(credentials), { headers: headers }).subscribe(res => {
        resolve(res.json());
      }, (err) => {
        this.storage.clear();
        this.createLoader('Restarting the Application ...');
        this.loading.present().then(() => {
          this.api.postData().then((result) => {
            if(result["message"] == true){
              this.storage.set('dynamicurl_utama',result["data"].url);
              this.storage.set('apikeyaccess',result["data"].apikeyaccess);
              this.storage.set('module_endpoint',result["data"].module);
              this.storage.get('userdatateknisi').then((val) => {
                if (val != null) { 
                  this.app.getRootNav().setRoot(BerandaPage);
                } else {
                  this.app.getRootNav().setRoot(LoginsPage);  
                }
              },(err) => {
                this.msg.msg_plainmsg(err);
              }); 
            }else{
              this.msg.msg_plainmsg('Enable get url, please call your IT Support');
            }
          });
          setTimeout(() => {
            this.dissmissloading();
          }, 3000);
        }, (err) => {
          this.dissmissloading();
          this.msg.msg_plainmsg(err);
        });
        
      });
    });
  }

}
/** */
