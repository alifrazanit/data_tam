import { Component } from '@angular/core';
import { MsgProvider } from '../../providers/msg/msg';
import { AlertController } from  'ionic-angular';
import { App,IonicPage, NavController, NavParams,LoadingController, Loading } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { Storage } from '@ionic/storage';
import { HeaderscanPage } from '../../pages/headerscan/headerscan';
import { ApiProvider } from '../../providers/api/api';
import { LogoutmidPage } from '../../pages/logoutmid/logoutmid';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { FindarrProvider } from '../../providers/findarr/findarr';

import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-dashboardwo',
  templateUrl: 'dashboardwo.html',
})
export class DashboardwoPage {
  dashboardwoheader:any;
  id_dashboardetail:any;
  datawo:any;
 
  lat:any;
  lng:any;
  userdata:any;
  apikeyaccess: any;
  menus: any;
    //searching contents
    public searching: any = false;
    public moreinfoteknisi:any=false;
    public dataexist:any=false;
    public btnstatus:any=false;
    public id_karyawan:any;
    public dissaktiftombol:any=false;
    public btnprogress:any=false;
    searchTerm: string = '';
    dataparticipant:any;
    dataretrieve: any;
    responseDataSearch: any;
    lokasi:any;
    dataSet:any;
    //searching contents
    loading: Loading;
  constructor(
    private alerts:AlertController,
    private service:ServiceProvider,
    public navCtrl: NavController, 
    public navParams: NavParams,
    private msg:MsgProvider,
    private api:ApiProvider,
    private localctrl:LocalstoragecontrollerProvider,
    private app:App,
    private storage:Storage,
    private loadingCtrl: LoadingController,
    public menuCtrl: MenuController,
    private findarr: FindarrProvider,
    private scanner: BarcodeScanner,
    private nativeGeocoder: NativeGeocoder,
    private geo: Geolocation,
    private locationAccuracy: LocationAccuracy
    
  ) {
    let tmpwo = this.navParams.get('dashboardwoheader');
    this.dashboardwoheader = tmpwo[0];
    this.datawo = this.navParams.get('datawo');
    this.storage.get('userdatateknisi').then((val) => {
      this.userdata = val[0];
      this.id_karyawan = val[0].Kode_Karyawan
      if(this.dashboardwoheader.open_by == this.id_karyawan){
        this.dissaktiftombol = true;
      }else{
        this.dissaktiftombol = false;
      }
    });
    this.storage.get('apikeyaccess').then((val) => {
      this.apikeyaccess = val;
    });
    
    this.storage.get('access').then((val) => {
      if (val != null) {
        
        if (val.toLowerCase() == "teknisi_am") {
          this.moreinfoteknisi = true;
        } 
      }else{
        this.localctrl.restart();
      }
    });
    this.storage.get('module_endpoint').then((val) => {
      this.menus = val;
    });
  }
  showprompt(dataparam){
    const confirm = this.alerts.create({
      title:'Logout Aplikasi',
      message:'Apakah Anda yakin ingin logout?',
      buttons:[
        {
          text:'Ya',
          handler:()=>{
            this.navCtrl.push(LogoutmidPage,{
              "dataparam":dataparam,
              "data_wo":dataparam,
              "id_dashboardetail":this.id_dashboardetail
            });
          }
        },
        {
          text:'Tidak',
          handler:()=>{
            //ga ngapa2in
          }
        }
      ]
    });
    confirm.present();
  }
  
 

  checkstatus(param) {
    let color = new String;
    if (param == '0') {//0 = UNREAD    ABU-ABU
      color = "onnotyet";
    } else if (param == '1') {//2 = PROGRESS	BIRU
      color = "onproccess";
    } else if (param == '2') {//   3 = CLOSE		  MERAH
      color = "onclose";
    }

    return color;
  }
  checkurgensiwrn(param) {
    let color = new String;
    if (param == '1') {//0 = UNREAD    ABU-ABU
      color = "energy";
    } else if (param == '2') {//2 = PROGRESS	BIRU
      color = "danger";
    } 
    return color;
  }
  checktext(param) {
    let txt = new String;
    if (param == '0') {//0 = blm proses    ABU-ABU
      txt = "NOT YET";
    } else if (param == '1') {//2 = PROGRESS	BIRU
      txt = "ON PROCESS";
    } else if (param == '2') {//   3 = CLOSE		MERAH
      txt = "CLOSED";
    }
    return txt;
  }
  checkurgensi(param) {
    let txt = new String;
    if (param == '1') {//0 = blm proses    ABU-ABU
      txt = "IMPORTANT";
    } else if (param == '2') {
      txt = "URGENT";
    }else{
      txt = "";
    }
    return txt;
  }
  changeimg(param){
    let imgsrc = new String;
    if(param == null || param == ""){
      imgsrc = "assets/imgs/iconcust.png";
    }else{
      imgsrc = param;
    }
    return imgsrc;
  }
  changeimgtek(param){
    let imgsrc = new String;
    if(param == null || param == ""){
      imgsrc = "assets/imgs/iconeng.png";
    }else{
      imgsrc = param;
    }
    return imgsrc;
  }
  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  onSearchInput() {
    this.searching = true;
  }
  filterItems(searchTerm) {
    if (searchTerm != null && searchTerm != "") {
      return this.dataretrieve.filter((item) => {
        return item.Nama_Karyawan.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
      })
    } else {
      return this.dataretrieve;
    }
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true, 'myMenu');
  }
  ionViewDidLoad() {
    this.loaddatasearch();
  }
  logout(data){
    this.showprompt(data);
  }
  loaddatasearch(){
    this.createLoader('Pulling Data ...');
    this.loading.present().then(() => {
      this.storage.get('dynamicurl_utama').then((val) => {
        if (val != null) {
          var nextsegment = null;
          if (this.findarr.searchmenu("Checkparticipant", this.menus) != null) {
            nextsegment = this.findarr.searchmenu("Checkparticipant", this.menus);
            var datapostcount = {
              "apikey": this.apikeyaccess,
              "dataheaderwo": this.dashboardwoheader,
              "userdata":this.userdata
            }
            
            this.service.postData(datapostcount, val,nextsegment["endpoint"]).then((result) => {
              if(result["message"] == true){
                if(result["data"] == null){
                }else{
                  this.id_dashboardetail = result["data"][0]["id_dashboard"];
                }
              }else{
                this.msg.msg_plainmsg(JSON.stringify(result["data"]));
              }
            });
            
            nextsegment = this.findarr.searchmenu("Pullparticipant", this.menus);
            this.service.postData(datapostcount, val,nextsegment["endpoint"]).then((result) => {
              if(result["message"] == true){
              if(result["data"] != null){
                  this.dataexist =false;
                }else{
                  this.dataexist =true;
                }
                 if(result["data"].length>1){
                  this.btnstatus = false;
                }else{
                  this.btnstatus = true;
                }
                 
                var datazz = this.findarr.searchmenuteknsii(this.id_karyawan, result["data"]);
                if(datazz == null){
                  this.btnprogress =true;
                }else{
                  if(datazz.logout_time != null){
                    this.btnprogress =true;
                    this.btnstatus = true;
                    this.dissaktiftombol = true;
                  }else{
                    this.dissaktiftombol = true;
                    this.btnprogress =false;
                  }
                }
                this.dataparticipant = result["data"];
              }else{
                this.msg.msg_plainmsg(JSON.stringify(result["data"]));
              }
            });
            this.dissmissloading();
          }else {
            this.dissmissloading();
            this.localctrl.restart();
          }
        }else {
          this.dissmissloading();
          this.localctrl.restart();
        }
      });
      setTimeout(() => {
        this.dissmissloading();
      }, 3000);
    }, (err) => {
      this.dissmissloading();
      this.msg.msg_plainmsg(err);
    });
  }
  detailwo(){
    this.navCtrl.push(HeaderscanPage,{
      "datawo":this.datawo,
      "dashboardheader":this.dashboardwoheader,
      "iddashboarddetail":this.id_dashboardetail
    })
  }
  doRefresh(event){
    this.dataSet = null;
    this.loaddatasearch();
    setTimeout(() => {
      event.complete();
    }, 1000);
  }
  joindashboard(databarcode,lang,lat,lokasi,data){
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("Joinwo", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("Joinwo", this.menus);
          var postparam = {
            barcode: databarcode,
            lng: lang,
            lat: lat,
            loc: lokasi,
            "userdata": this.userdata,
            "apikey": this.apikeyaccess,
            "datatiket": data
          }
          this.createLoader('Posting Data ...');
          this.loading.present().then(() => {
            this.service.postData(postparam, val, nextsegment["endpoint"]).then((result) => {
              if(result["message"] == false){
                this.msg.msg_plainmsg(result["data"]);
              }
              this.dissmissloading();
              this.loaddatasearch();
            });
            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        }else{
          this.localctrl.restart();
        }
      }else{
 this.localctrl.restart();
      }
    });
  }
  scanbarcode(datawo) {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
          this.geo.getCurrentPosition().then((pos) => {
            this.lat = pos.coords.latitude;
            this.lng = pos.coords.longitude;
            this.nativeGeocoder.reverseGeocode(this.lat, this.lng)
              .then((result: NativeGeocoderReverseResult[]) => {
                this.lokasi = JSON.stringify(result);
                this.scanner.scan().then(barcodeData => {
                  if (barcodeData.text != "") {
                    this.joindashboard(barcodeData.text, this.lng, this.lat, this.lokasi, datawo);
                  }
                }).catch(err => {
                  this.msg.msg_plainmsg(err);
                });
              }).catch((error) => {
                this.msg.msg_plainmsg("#4Error " + error);
              });
          }).catch((error) => {
            this.msg.msg_plainmsg("#3Error " + error);
          });
        }).catch((error) => {
          this.msg.msg_plainmsg("#2Error " + error);
        });
    }).catch((error) => {
      this.msg.msg_plainmsg("#1Error " + error);
    });
  }
}
