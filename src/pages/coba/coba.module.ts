import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CobaPage } from './coba';

@NgModule({
  declarations: [
    CobaPage,
  ],
  imports: [
    IonicPageModule.forChild(CobaPage),
  ],
})
export class CobaPageModule {}
