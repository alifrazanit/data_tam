import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App, LoadingController, Loading } from 'ionic-angular';
import { AddsparepartPage } from '../../pages/addsparepart/addsparepart';
import { MsgProvider } from '../../providers/msg/msg';
import { AdderrorPage } from '../../pages/adderror/adderror';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';
import { FindarrProvider } from '../../providers/findarr/findarr';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { FrhdPage } from '../../pages/frhd/frhd';
import { FrroPage } from '../../pages/frro/frro';

@IonicPage()
@Component({
  selector: 'page-detailscan',
  templateUrl: 'detailscan.html',
})
export class DetailscanPage {
  datascan: any;

  aktifbtnhd : any;
  aktifbtnro : any;menus:any;userdata:any;apikeyaccess:any;
  public no_seri: String;
  public no_ref: String;
  public id_scan_header: any;
  public id_dashboard_detail: any;
  public bisnis:any;
  loading: Loading;
  dataSet:any;
  public hd:any;
  public ro:any;
  public servis:any;
  constructor(
    private app: App,
    private loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private localctrl: LocalstoragecontrollerProvider,
    private msg: MsgProvider,
    private findarr: FindarrProvider,
    private service:ServiceProvider,
    private storage: Storage
  ) {
    this.datascan = this.navParams.get('datascan');
    this.no_seri = this.datascan["no_seri"];
    this.no_ref = this.datascan["no_ref"];
    this.id_dashboard_detail = this.datascan["id_dashboard_detail"];
    this.id_scan_header = this.datascan["id_scan_header"];
    this.setIdscanheader(this.id_scan_header);
    this.storage.get('module_endpoint').then((val) => {
      this.menus = val;
    });
    this.storage.get('userdatateknisi').then((val) => {
      this.userdata = val;
    });
    this.storage.get('apikeyaccess').then((val) => {
      this.apikeyaccess = val;
    });
   
    
  }
  setIdscanheader(id_scan_headerv:any){
    this.id_scan_header = id_scan_headerv;
  }
  getIdscanheader():any{
    return this.id_scan_header;
  }
  doRefresh(event) {
    this.dataSet = null;
    this.loaddatasearch();
    setTimeout(() => {
      event.complete();
    }, 1000);
  }
  dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  ionViewDidLoad() {
    this.loaddatasearch();
  }
  openModalSp() {
    this.navCtrl.push(AddsparepartPage, {
      no_seri :this.no_seri,
      no_ref :this.no_ref,
      id_dashboard_detail :this.id_dashboard_detail,
      id_scan_header :this.id_scan_header
    });
  }
  
  openModalErr(){
    this.navCtrl.push(AdderrorPage, {
      no_seri :this.no_seri,
      no_ref :this.no_ref,
      id_dashboard_detail :this.id_dashboard_detail,
      id_scan_header :this.id_scan_header
    });
    
  }
  loaddatasearch() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("GetinformationBusiness", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("GetinformationBusiness", this.menus);
          let datapostcount = {
            "apikey": this.apikeyaccess,
            "id_scan_header":this.id_scan_header
          };
          this.createLoader('Check Information ...');
          this.loading.present().then(() => {
            this.service.postData(datapostcount, val, nextsegment["endpoint"]).then((result) => {
              if(result["data"][0]["jenismesin"] == '1'){
                this.aktifbtnhd = false;
                this.aktifbtnro = true;
              }else if(result["data"][0]["jenismesin"] == '2'){
                this.aktifbtnhd = true;
                this.aktifbtnro = false;
              }else{
                this.aktifbtnhd = false;
                this.aktifbtnro = false;
              }
            });
          setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        }else{
          this.localctrl.restart();
        }
      }else{
        this.localctrl.restart();
      }
    });


  }
  openfrhd(){
    this.navCtrl.push(FrhdPage,{
      "id_scan_header":this.id_scan_header
    });
  }
  openfrro(){
    this.navCtrl.push(FrroPage,{
      "id_scan_header":this.id_scan_header
    });
  }
}
