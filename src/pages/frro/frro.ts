import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading,ToastController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { FindarrProvider } from '../../providers/findarr/findarr';
import { MenuController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { MsgProvider } from '../../providers/msg/msg';

@IonicPage()
@Component({
  selector: 'page-frro',
  templateUrl: 'frro.html',
})
export class FrroPage {
  frro= {};//array
  loading: Loading;
  menus: any;
  private userdata: any;
  apikeyaccess: any;
  id_scan_header: any;
  constructor(
    private toast:ToastController,
    private msg: MsgProvider,
    private service: ServiceProvider,
    private menuCtrl: MenuController,
    private localctrl: LocalstoragecontrollerProvider,
    private findarr: FindarrProvider,
    private loadingCtrl: LoadingController,
    private storage: Storage,
    public navCtrl: NavController, public navParams: NavParams) {
    this.id_scan_header = this.navParams.get('id_scan_header');
    this.storage.get('module_endpoint').then((val) => {
      this.menus = val;
    });
    this.storage.get('userdatateknisi').then((val) => {
      this.userdata = val;
    });
    this.storage.get('apikeyaccess').then((val) => {
      this.apikeyaccess = val;
    });
    this.menuCtrl.enable(true, 'myMenu');
  }
  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  public dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  ionViewDidLoad() {
    this.loaddata();
  }
  filldata(data) {
    if(data.data != ""){

      var vdata = data.data[0];
      console.log(data.data[0]["Id_scan_ro"]);
      this.frro["Id_scan_ro"] = vdata["Id_scan_ro"];
      this.frro["id_scan_header"] = vdata["id_scan_header"];
      this.frro["kalibrasi"] = vdata["kalibrasi"];
      this.frro["tgl_instalasi"] = vdata["tgl_instalasi"];
      this.frro["feed_pressure"] = vdata["feed_pressure"];
      this.frro["system_pressure"] = vdata["system_pressure"];
      this.frro["permeate_flow"] = vdata["permeate_flow"];
      this.frro["permeate_temperature"] = vdata["permeate_temperature"];
      this.frro["permeate_conductivity"] = vdata["permeate_conductivity"];
      this.frro["concentrate_flow"] = vdata["concentrate_flow"];
      this.frro["concentrate_conductivity"] = vdata["concentrate_conductivity"];
      this.frro["feed_water_flow"] = vdata["feed_water_flow"];
      this.frro["feed_water_conductivity"] = vdata["feed_water_conductivity"];
      this.frro["feed_water_temperature"] = vdata["feed_water_temperature"];
      this.frro["leaking_ro"] = vdata["leaking_ro"];
      this.frro["pompa_tekanan_tinggi"] = vdata["pompa_tekanan_tinggi"];
      this.frro["instrumen_listrik"] = vdata["instrumen_listrik"];
      this.frro["mechanical_instrument"] = vdata["mechanical_instrument"];
      this.frro["ganti_module_membrane"] = vdata["ganti_module_membrane"];
      this.frro["backwash_sand_filter"] = vdata["backwash_sand_filter"];
      this.frro["backwash_carbon_filter"] = vdata["backwash_carbon_filter"];
      this.frro["regenerasi_resin"] = vdata["regenerasi_resin"];
      this.frro["check_kebocoran_piping"] = vdata["check_kebocoran_piping"];
      this.frro["penggantian_filter_cartridges"] = vdata["penggantian_filter_cartridges"];
      this.frro["penggantian_sand_filter"] = vdata["penggantian_sand_filter"];
      this.frro["penggantian_resin_filter"] = vdata["penggantian_resin_filter"];
      this.frro["penggantian_carbon_filter"] = vdata["penggantian_carbon_filter"];
      this.frro["check_kebocoran_pompa_filtrasi"] = vdata["check_kebocoran_pompa_filtrasi"];
      this.frro["check_instrumen_listrik"] = vdata["check_instrumen_listrik"];
      this.frro["check_kebocoran_pompa_feed"] = vdata["check_kebocoran_pompa_feed"];
      this.frro["check_instrumen_mekanical"] = vdata["check_instrumen_mekanical"];
      this.frro["rencana"] = vdata["rencana"];
      this.frro["catatan"] = vdata["catatan"];

    }
  }


  loaddata() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("LoadRoScan", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("LoadRoScan", this.menus);
          let datapost = {
            "apikey": this.apikeyaccess,
            "id_scan_header": this.id_scan_header
          };
          this.createLoader('Loading ...');
          this.loading.present().then(() => {
            this.service.postData(datapost, val, nextsegment["endpoint"]).then((result) => {
              this.dissmissloading();
              this.filldata(result);
            });
            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        }else{
          this.localctrl.restart();
        }
      }else{
        this.localctrl.restart();
      }
    });
  }
  simpanformro(){
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        var nextsegment2 = null;
        if (this.findarr.searchmenu("SaveROScan", this.menus) != null || this.findarr.searchmenu("UpdateRoScan", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("SaveROScan", this.menus);
          nextsegment2 = this.findarr.searchmenu("UpdateROScan", this.menus);
          let datapost = {
            "data": this.frro,
            "id_scan_ro": this.frro["Id_scan_ro"],
            "id_scan_header": this.id_scan_header,
            "apikey": this.apikeyaccess,
          };
          this.createLoader('Loading ...');
          this.loading.present().then(() => {
            if (datapost["id_scan_ro"] == null) {
              this.service.postData(datapost, val, nextsegment["endpoint"]).then((result) => {
                if(result["message"]==true){
                  this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                
                  this.navCtrl.pop();
                }else{
                  this.msg.msg_plainmsg(result["data"]);
                }
                this.dissmissloading();
              });
            }else if(datapost["id_scan_ro"] != null) {
              this.service.postData(datapost, val, nextsegment2["endpoint"]).then((result) => {
                if(result["message"]==true){
                  this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                
                  this.navCtrl.pop();
                }else{
                  this.msg.msg_plainmsg(result["data"]);
                }
                this.dissmissloading();
              });
            }

            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        }else{
          this.localctrl.restart();
        }
      }else{
        this.localctrl.restart();
      }
    });

  }
}
