import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QtyspmodalPage } from './qtyspmodal';

@NgModule({
  declarations: [
    QtyspmodalPage,
  ],
  imports: [
    IonicPageModule.forChild(QtyspmodalPage),
  ],
})
export class QtyspmodalPageModule {}
