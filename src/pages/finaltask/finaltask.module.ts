import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinaltaskPage } from './finaltask';

@NgModule({
  declarations: [
    FinaltaskPage,
  ],
  imports: [
    IonicPageModule.forChild(FinaltaskPage),
  ],
})
export class FinaltaskPageModule {}
