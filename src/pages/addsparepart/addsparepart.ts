import { Component, ViewChild } from '@angular/core';
import { ModalController, App, IonicPage, NavController, NavParams, ToastController, LoadingController, Loading } from 'ionic-angular';
import 'rxjs/add/operator/debounceTime';
import { Storage } from '@ionic/storage';
import { IonicSelectableComponent } from 'ionic-selectable';
import { QtyspmodalPage } from '../../pages/qtyspmodal/qtyspmodal';
import { MsgProvider } from '../../providers/msg/msg';
import { ServiceProvider } from '../../providers/service/service';
import { ApiProvider } from '../../providers/api/api';
import { LoginsPage } from '../logins/logins';
import { FindarrProvider } from '../../providers/findarr/findarr';
import { LocalstoragecontrollerProvider } from '../../providers/localstoragecontroller/localstoragecontroller';
import { AlertController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-addsparepart',
  templateUrl: 'addsparepart.html',
})
export class AddsparepartPage {

  @ViewChild('myselect') selectComponent: IonicSelectableComponent;
  Sparepart = null;
  btnaktif: any = false;
  menus: any; userdata: any; apikeyaccess: any;
  SparepartList = [];
  loading: Loading;
  dataSparepart: any;
  SparepartId: any;
  dataspareparttersimpan: any;

  public qtyjml: any;
  no_seri: any;
  no_ref: any;
  id_dashboard_detail: any;
  id_scan_header: any;
  public kode_karyawan: any;
  public dataset: any;
  constructor(
    private alerts: AlertController,
    private findarr: FindarrProvider,
    private localctrl: LocalstoragecontrollerProvider,
    private msg: MsgProvider,
    private api: ApiProvider,
    private service: ServiceProvider,
    private toast: ToastController,
    private loadingCtrl: LoadingController,
    private storage: Storage,
    public navCtrl: NavController,
    private app: App,
    private modal: ModalController,
    public navParams: NavParams) {
    this.no_seri = this.navParams.get('no_seri');
    this.no_ref = this.navParams.get('no_ref');
    this.id_dashboard_detail = this.navParams.get('id_dashboard_detail');
    this.id_scan_header = this.navParams.get('id_scan_header');

    this.storage.get('module_endpoint').then((val) => {
      this.menus = val;
    });
    this.storage.get('userdatateknisi').then((val) => {
      this.userdata = val;
    });
    this.storage.get('apikeyaccess').then((val) => {
      this.apikeyaccess = val;
    });
  }

  SparepartChanged(event: { component: IonicSelectableComponent, value: any }) {
    console.log('event :', event);
  }
  onClose(param) {
    let toatsz = this.toast.create({
      message: 'Thanks for your selection',
      duration: 2000
    });
    toatsz.present();

  }
  openFromCode() {
    this.selectComponent.open();
  }


  public createLoader(message: any) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }
  public dissmissloading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }


  ionViewDidLoad() {
    this.getcomsparepart();
  }
  ionViewWillEnter() {
    this.loadSparepart();
  }

  getcomsparepart() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("LoadDataSparepart", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("LoadDataSparepart", this.menus);
          var datapostcount = {
            "apikey": this.apikeyaccess,
            "keywords": "pulling_data_sparepart"
          }
          this.storage.get('data_sparepart').then((resulstsp) => {
            if (resulstsp != null) {
              this.SparepartList = resulstsp;
            }
            else {
              this.service.postData(datapostcount, val, nextsegment["endpoint"]).then((result) => {
                if (result["message"] == true) {
                  if (result["type"] == "data_sparepart") {
                    let getdat = result['data'];
                    this.SparepartList = getdat;
                    this.storage.set('data_sparepart', getdat);
                  }
                } else {
                  this.msg.msg_plainmsg(JSON.stringify(result['data']));
                }
              });
            }
          });
        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });
  }

  simpansparepart() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("SaveSparepart", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("SaveSparepart", this.menus);
          if (this.SparepartId != null) {
            let postparam = {
              id_scan_header: this.id_scan_header,
              kd_sparepart: this.SparepartId,
              "apikey": this.apikeyaccess,
              "no_ref": this.no_ref,
              "id_dashboard_detail": this.id_dashboard_detail,
              "kode_karyawan": this.kode_karyawan
            }
            this.createLoader('Saving Data ...');
            this.loading.present().then(() => {
              this.service.postData(postparam, val, nextsegment["endpoint"]).then((result) => {
                if (result["message"] == true) {
                  this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                  this.navCtrl.pop();
                } else {
                  this.msg.msg_plainmsg(JSON.stringify(result["data"]));
                }
              });
              setTimeout(() => {
                this.dissmissloading();
              }, 3000);
            }, (err) => {
              this.dissmissloading();
              this.msg.msg_plainmsg(err);
            });
          }

        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });
  }
  loadSparepart() {
    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("LoaddatatableSparepart", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("LoaddatatableSparepart", this.menus);
          let postparam = {
            id_scan_header: this.id_scan_header,
            "apikey": this.apikeyaccess
          }
          this.createLoader('Loading ...');
          this.loading.present().then(() => {
            this.service.postData(postparam, val, nextsegment["endpoint"]).then((result) => {
              if (result["message"] == true) {
                this.dataset = result["data"];
                this.qtyjml = result["data"].length;
                this.dissmissloading();
              } else {
                this.msg.msg_plainmsg(JSON.stringify(result["data"]));
                this.dissmissloading();
              }
            });
            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });
        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });

  }
  openmodal(dataall) {
    var mymodal = this.modal.create(QtyspmodalPage, {
      data: dataall,
      no_ref: this.no_ref,
      id_dashboard_detail: this.id_dashboard_detail
    }, { cssClass: "modal-setengah" });
    mymodal.present();
  }
  doRefresh(event) {
    this.dataset = null;
    this.loadSparepart();
    setTimeout(() => {
      event.complete();
    }, 1000);
  }
  showprompt(dataparam) {
    const confirm = this.alerts.create({
      title: 'Delete Data',
      message: 'Apakah Anda yakin ingin menghapus data ' + dataparam["kd_sparepart"] + '?',
      buttons: [
        {
          text: 'Ya',
          handler: () => {
            this.deletesparepart(dataparam);
          }
        },
        {
          text: 'Tidak',
          handler: () => {
            //ga ngapa2in
          }
        }
      ]
    });
    confirm.present();
  }

  deletesparepart(data) {

    this.storage.get('dynamicurl_utama').then((val) => {
      if (val != null) {
        var nextsegment = null;
        if (this.findarr.searchmenu("DeleteSparepart", this.menus) != null) {
          nextsegment = this.findarr.searchmenu("DeleteSparepart", this.menus);
          let postparam = {
            data: data,
            no_ref: this.no_ref,
            id_dashboard_detail: this.id_dashboard_detail,
            "apikey": this.apikeyaccess,
          }
          this.createLoader('Deleting ...');
          this.loading.present().then(() => {
            this.service.postData(postparam, val, nextsegment["endpoint"]).then((result) => {
              if (result["message"] == true) {
                this.toast.create({ message: result["data"], duration: 3000, position: 'top' }).present();
                this.loadSparepart();
                this.dissmissloading();
              } else {
                this.msg.msg_plainmsg(JSON.stringify(result["data"]));
                this.dissmissloading();
              }
            });
            setTimeout(() => {
              this.dissmissloading();
            }, 3000);
          }, (err) => {
            this.dissmissloading();
            this.msg.msg_plainmsg(err);
          });

        } else {
          this.localctrl.restart();
        }
      } else {
        this.localctrl.restart();
      }
    });
  }
}
