import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TtdwoPage } from './ttdwo';

@NgModule({
  declarations: [
    TtdwoPage,
  ],
  imports: [
    IonicPageModule.forChild(TtdwoPage),
  ],
})
export class TtdwoPageModule {}
