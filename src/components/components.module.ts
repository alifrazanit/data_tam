import { NgModule } from '@angular/core';
import { FrroComponent } from './frro/frro';
import { FrhdComponent } from './frhd/frhd';
@NgModule({
	declarations: [FrroComponent,
    FrhdComponent,
    FrhdComponent,
    FrhdComponent],
	imports: [],
	exports: [FrroComponent,
    FrhdComponent,
    FrhdComponent,
    FrhdComponent]
})
export class ComponentsModule {}
